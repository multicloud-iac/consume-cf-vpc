# Provision AWS Instance into VPC ID read from CloudFormation Stack

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.46.0"
    }
  }
  required_version = ">= 0.13"
}

# INITIALIZE AWS PROVIDER
provider "aws" {
  region = "us-west-2"
}

# GET OUTPUTS OF CLOUDFORMATION STACK
data "aws_cloudformation_stack" "vpc" {
  name = var.cf_stack_name
}

# CREATE SUBNET IN VPC READ FROM CF
resource "aws_subnet" "placement" {
  vpc_id            = data.aws_cloudformation_stack.vpc.outputs["VPCId"]
  cidr_block        = var.subnet_cidr
  availability_zone = "us-west-2a"

  tags = {
    Name    = "${var.name_prefix}-subnet-${join("", regex("^(\\d+)\\.\\d+\\.\\d+\\..*", var.subnet_cidr))}"
    Project = var.name_prefix
  }
}

# FIND MOST RECENT IMAGE
data "aws_ami" "most_recent_ami" {
  filter {
    name   = "name"
    values = [var.ami_search_name]
  }

  most_recent = true

  owners = [var.ami_owner]
}

# CREATE AWS INSTANCE
resource "aws_instance" "aws_vm" {
  availability_zone           = "us-west-2a"
  subnet_id                   = aws_subnet.placement.id
  associate_public_ip_address = true

  ami           = data.aws_ami.most_recent_ami.id
  instance_type = var.instance_type

  tags = {
    Name    = "${var.name_prefix}-instance"
    Project = var.name_prefix
  }
}

# DISPLAY PUBLIC IP ADDRESS
output "instance_ip_address" {
  description = "Instance IP Address"
  value       = aws_instance.aws_vm.public_ip
}
