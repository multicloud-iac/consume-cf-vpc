# consume-cf-vpc

> CloudFormation Example 2 - Deploy Resources into VPC created by CloudFormation stack

## Dependencies

- Ensure the `deploy-cf-vpc` example has been provisioned before running.

## Data and Resources

- Read CloudFormation Stack outputs from the VPC deployed in previous step using `aws_cloudformation_stack` data source.
- Deploy a subnet in that VPC.
- Deploy an instance into subnet.

## Notable Variables

- `name_prefix` - Friendly name prefix used when naming resources.
- `cf_stack_name` - Name of CF Stack deployed by `deploy-cf-vpc`
  - This value in this repo must be the same as `deploy-cf-vpc`
  - default: `test-vpc-172`
- `subnet_cidr` - IP CIDR for subnet created in VPC.
  - Must be a valid subnet CIDR for the `vpc_cidr` in `deploy-cf-vpc` repo
  - default: `172.16.10.0/24`

## Notes

- If `subnet_cidr` value is changed
  - Must also change `vpc_cidr` in `deploy-cf-vpc` repo to a valid CIDR that can contain this subnet.
- `cf_stack_name` must have the same value in both repos `deploy-cf-vpc` and `consume-cf-vpc`

## Tear down

- Destroy the infrastructure created in this repo before destroying the CloudFormation VPC stack created in `consume-cd-vpc`
