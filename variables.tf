variable "name_prefix" {
  description = "Friendly name prefix used when naming resources."
  default     = "tfcb-test"
}

variable "cf_stack_name" {
  description = "Name of CF Stack that created VPC where subnet will be created."
  default     = "test-vpc-172"
}

variable "subnet_cidr" {
  description = "IP CIDR for subnet created in VPC."
  default     = "172.16.10.0/24"
}

variable "instance_type" {
  description = "type of EC2 instance to provision."
  default     = "t2.micro"
}

variable "ami_owner" {
  description = "Owner of AMI to provision."
  default     = "099720109477"
}

variable "ami_search_name" {
  description = "Search name of AMI to provision."
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"
}
